#!/bin/bash

# Set up ssh-keys
mkdir .ssh
cat << EOF > .ssh/authorized_keys
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKYDYJTxld1+XjA3QIbeV+dl9NmdqhiiUQtu4ZL1zhY4 haydn@thedemonrebornlinux
EOF
chmod 0600 .ssh/authorized_keys 
chmod 0700 .ssh/

# Set up bashrc and bash_profile
cat << EOF > .bashrc
# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
alias 'ls'='ls -l --color=auto'
alias 'l'='ls -l --color=auto'

export EDITOR="/usr/bin/vim"
EOF

centos_ver=`uname -or | sed 's/.*el\([0-9]\).*/\1/'`

if [ $centos_ver -eq '8' ]; then
    sudo yum -y install vim sqlite bash-completion
else
    sudo yum -y install vim epel-release sqlite bash-completion bash-completion-extras
fi

# Install and configure vim
cat << EOF > .vimrc
filetype plugin indent on

syntax on

set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab

set smartcase
set incsearch

set autoindent

set autoread
set scrolloff=3

set wildmode=longest,list,full
set wildmenu

set hidden

set nowrap

nnoremap <F2> :ls<CR>:b<SPACE>
nnoremap <F3> :/\s\+$<CR>
nnoremap <F4> :bp\|bd #<CR>

nnoremap <A-Left> :bn<CR>
nnoremap <A-Right> :bp<CR>
inoremap <A-Left> <ESC>:bn<CR>
inoremap <A-Right> <ESC>:bp<CR>

nnoremap <C-S-Left> <C-W><Left>
nnoremap <C-S-Right> <C-W><Right>
inoremap <C-S-Left> <ESC><C-W><Left>
inoremap <C-S-Right> <ESC><C-W><Right>
EOF

sudo yum -y update

sudo reboot
