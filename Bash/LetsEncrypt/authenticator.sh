#!/bin/sh

$filename=/path/to/dns/file

sed -i "s#_acme-challenge	300	IN	TXT	.*#_acme-challenge	300	IN	TXT	\"$CERTBOT_VALIDATION\"#g" $filename

systemctl restart named

