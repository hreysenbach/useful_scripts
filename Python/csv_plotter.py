#!/usr/bin/python3

import sys
import getopt
import matplotlib.pyplot
import csv
import numpy

class SnaptoCursor(object):
    """
    Like Cursor but the crosshair snaps to the nearest x,y point
    For simplicity, I'm assuming x is sorted
    """

    def __init__(self, ax, x, y, curs_num) :
        self.ax = ax
        self.lx = ax.axhline(y=min(y), color='k')  # the horiz line
        self.ly = ax.axvline(x=min(x), ymin=min(y), ymax=max(y), color='k')  # the vert line
        self.x = x
        self.y = y
        # text location in axes coords
        self.txt = ax.text(0.7, 0.9 - curs_num / 20, '', transform=ax.transAxes)

    def mouse_move(self, event) :

        if not event.inaxes:
            return

        x, y = event.xdata, event.ydata

        indx = min(numpy.searchsorted(self.x, [x])[0], len(self.x) - 1)
        x = self.x[indx]
        y = self.y[indx]
        # update the line positions
        self.lx.set_ydata(y)
        self.ly.set_xdata(x)

        self.txt.set_text('x=%1.2f, y=%1.2f' % (x, y))
        matplotlib.pyplot.draw()

def print_help() :
    print("Usage: csv_plotter.py -i [INPUT_FILE] -x [X_AXIS] -y [Y_AXIS] [OPTION]")
    print("Plots a CSV file")
    print("Example: csv_plotter.py -i test_file.csv -x 0 -y 1,2")
    print("")
    print("Required Arguments")
    print("  -i, --input_file          The CSV file to plot")
    print("  -x, --x-axis              The column to use for the x-axis")
    print("  -y, --y-axis              The columns to use for the y-axis")
    print("")
    print("Optional Arguments")
    print("      --start               The first row to plot")
    print("      --stop                The last row to plot")

def main() :
    long_opts = [
            "input_file=",
            "x-axis=",
            "y-axis=",
            "start=",
            "stop=",
    ]

    input_file = None
    x_col = None
    y_col = None

    try :
        opts, args = getopt.getopt(sys.argv[1:], "hi:x:y:", long_opts)
    except getopt.GetoptError :
        print_help()
        sys.exit(2)

    for opt, arg in opts :
        if (opt == "-h") :
            print_help()
            sys.exit(0)
        elif (opt in ("-i", "--input_file")) :
            input_file = arg
        elif (opt in ("-x", "--x-axis")) :
            x_col = int(arg)
        elif (opt in ("-y", "--y-axis")) :
            y_col_strings = arg.split(',')
            y_col = []
            for string in y_col_strings : 
                y_col.append(int(string))
        elif (opt in "--start") :
            start_row = int(arg)
        elif (opt in "--stop") :
            stop_row = int(arg)

    if (input_file == None or x_col == None or y_col == None) :
        print_help()
        sys.exit(0)

    fig, ax = matplotlib.pyplot.subplots()
    data = {}
    data["x"] = []
    for y in y_col:
        data["y{0}".format(y)] = []

    with open(input_file, newline='') as reading_file :
        reader = csv.reader(reading_file)
        for row in reader :
            data["x"].append(int(row[x_col]))
            for index, y in enumerate(y_col) :
                data["y{0}".format(y)].append(int(row[y]))

    for y in y_col :
        ax.plot(data["x"], data["y{0}".format(y)])

#    cursor0 = SnaptoCursor(ax, data["x"], data["y{0}".format(y_col[0])], 0)
#    cursor1 = SnaptoCursor(ax, data["x"], data["y{0}".format(y_col[1])], 1)
#    matplotlib.pyplot.connect("motion_notify_event", cursor0.mouse_move)
#    matplotlib.pyplot.connect("button_press_event", cursor1.mouse_move)
    
    matplotlib.pyplot.show()

if (__name__ == "__main__") :
    main()
