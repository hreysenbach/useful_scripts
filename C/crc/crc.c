

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <getopt.h>

#define CRC8_POLY               0x07
#define CRC16_CCIT_ZERO_POLY    0x1021
#define CRC32_ETH_POLY          0x4C11DB7

static struct option long_options [] = {
    { "crc",        required_argument, NULL, 'c'},
    { "filename",   required_argument, NULL, 'f'}
};

uint8_t reverse_bits(uint8_t byte)
{
    byte = (byte & 0xF0) >> 4 | (byte & 0x0F) << 4;
    byte = (byte & 0xCC) >> 2 | (byte & 0x33) << 2;
    byte = (byte & 0xAA) >> 1 | (byte & 0x55) << 1;

    return byte;
}

uint32_t reverse_32bits(uint32_t integer)
{
    uint32_t tmp;

    tmp = reverse_bits((integer & 0xFF000000) >> 24)
        | (reverse_bits((integer & 0x00FF0000) >> 16) << 8)
        | (reverse_bits((integer & 0x0000FF00) >> 8) << 16)
        | (reverse_bits(integer & 0x000000FF) << 24);

    return tmp;
}

int gen_crc8_table(uint8_t poly, uint8_t *crc_table)
{
    uint8_t remainder;

    for (uint16_t dividend = 0; dividend < 256; dividend++) {
        remainder = dividend;
        for (int j = 0; j < 8; j++) {
            if (remainder & 0x80) {
                remainder = remainder << 1;
                remainder = remainder ^ poly;
            } else {
                remainder = remainder << 1;
            }
        }
        crc_table[dividend] = remainder;
    }

    return 0;
}

int gen_crc16_table(uint16_t poly, uint16_t *crc_table)
{
    uint16_t remainder;

    for (uint16_t dividend = 0; dividend < 256; dividend++) {
        remainder = dividend << 8;
        for (int j = 0; j < 8; j++) {
            if (remainder & 0x8000) {
                remainder = remainder << 1;
                remainder = remainder ^ poly;
            } else {
                remainder = remainder << 1;
            }
        }
        crc_table[dividend] = remainder;
    }

    return 0;
}

int gen_crc32_table(uint32_t poly, uint32_t *crc_table)
{
    uint32_t remainder;

    for (uint16_t dividend = 0; dividend < 256; dividend++) {
        remainder = dividend << 24;
        for (int j = 0; j < 8; j++) {
            if (remainder & 0x80000000) {
                remainder = remainder << 1;
                remainder = remainder ^ poly;
            } else {
                remainder = remainder << 1;
            }
        }
        crc_table[dividend] = remainder;
    }

    return 0;
}

uint8_t calc_crc8(uint8_t *crc_table, uint8_t crc, uint8_t *buffer, uint32_t len)
{
    uint8_t *ptr;
    uint8_t *end;

    end = (uint8_t *) (buffer + len);

    for (ptr = (uint8_t *) buffer; ptr < end; ptr++) {
        crc = (crc << 8) ^ crc_table[(crc & 0xFF) ^ *ptr];
    }

    return crc;
}

uint16_t calc_crc16(uint16_t *crc_table, uint16_t crc, uint8_t *buffer, uint32_t len)
{
    uint8_t *ptr;
    uint8_t *end;

    end = (buffer + len);

    for (ptr = buffer; ptr < end; ptr++) {
        crc = (crc << 8) ^ crc_table[(((crc >> 8) ^ *ptr) & 0xFF)];
    }

    return crc;
}

uint32_t calc_crc32(uint32_t *crc_table, uint32_t crc, uint8_t *buffer, uint32_t len)
{
    uint8_t *ptr;
    uint8_t *end;

    end = (buffer + len);

    for (ptr = buffer; ptr < end; ptr++) {
        crc = (crc << 8) ^ crc_table[(((crc >> 24) ^ *ptr) & 0xFF)];
    }

    return crc;
}

void print_help(void)
{
    printf("crc --crc <CRC> --filename <FILE>\n");
    printf("\tValid CRC choices are 8, 16 or 32 to perform CRC8-CCITT, CRC16-CCITT-ZERO or CRC32-ETH respectively\n");
    printf("\tFILE specifies the file to perform the CRC on\n");

    return;
}

int main(int argc, char **argv)
{
    uint8_t     crc_sel;
    char        filename[128];
    char        c;
    FILE        *fp;

    uint8_t     buffer[1024*1024];
    uint32_t    num_of_bytes;
    uint32_t    crc;

    uint8_t     crc8_table[256];
    uint16_t    crc16_table[256];
    uint32_t    crc32_table[256];

    gen_crc8_table(CRC8_POLY, crc8_table);
    gen_crc16_table(CRC16_CCIT_ZERO_POLY, crc16_table);
    gen_crc32_table(CRC32_ETH_POLY, crc32_table);

    while ((c = getopt_long(argc, argv, "c:f:", long_options, NULL)) != -1) {
        switch (c) {
        case ('c'):
            if (strtol(optarg, NULL, 0) == 8) {
                crc_sel = 8;
            } else if (strtol(optarg, NULL, 0) == 16) {
                crc_sel = 16;
            } else if (strtol(optarg, NULL, 0) == 32) {
                crc_sel = 32;
            } else {
                printf("Invalid CRC specified!\n");
                print_help();
                return -1;
            }
            break;
        case ('f'):
            strcpy(filename, optarg);
            break;
        default:
            print_help();
            return -1;
            break;
        }
    }

    fp = fopen(filename, "rb");

    if (fp == NULL) {
        printf("Could not open file\n");
        return -1;
    }

    num_of_bytes = fread(buffer, sizeof(*buffer), sizeof(buffer), fp);

    if (ferror(fp)) {
        printf("Error reading from file\n");
        return -1;
    }

    if (crc_sel == 8) {
        crc = calc_crc8(crc8_table, 0x0, buffer, num_of_bytes);
        printf("Calculated CRC8 = 0x%02X\n", crc);
    } else if (crc_sel == 16) {
        crc = calc_crc16(crc16_table, 0x0, buffer, num_of_bytes);
        printf("Calculated CRC16 = 0x%04X\n", crc);
    } else if (crc_sel == 32) {
        for (int i = 0; i < num_of_bytes; i++) {
            buffer[i] = reverse_bits(buffer[i]);
        }
        crc = calc_crc32(crc32_table, 0xFFFFFFFF, buffer, num_of_bytes);
        crc = reverse_32bits(crc) ^ 0xFFFFFFFF;
        printf("Calculated CRC32 = 0x%08X\n", crc);
    }


    return 0;
}
