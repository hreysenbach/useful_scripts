
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


void usage(void)
{
	printf("resistor_calc -o VOLTAGE -r REF_VOLTAGE [-r2 RESISTOR_2_VALUE] [-e ERROR] [-E E_SERIES]\n\r");
	printf("Used to calculate required resistors for a voltage regulator\n\r");
	printf("\tVOLTAGE is the desired output voltage\n\r");
	printf("\tREF_VOLTAGE is the reference voltage for the regulator\n\r");
	printf("\tRESISTOR_2_VALUE is the value of the resistor on the bottom of the resistor divider\n\r");
	printf("\tERROR is the allowed amount of error in percentage. Defaults to 1%%\n\r");
	printf("\tE_SERIES is which range of resistors to choose from, current values are 192 and 96. Default is 96\n\r");

	return;
}

int parse_command_arguments(
	int argc, char** argv, double* desired_voltage, double* reference_voltage,
	int* resistor_2, double* allowed_error, int* E_series)
{
	int i = 1;
	int val = 0;
	int found = 0;

	if (argc < 5) {
		usage();
		return -1;
	}

	while (i < argc) {
		found = 0;

		val = strcmp(argv[i], "-o");
		if (val == 0) {
			found = 1;
			*desired_voltage = atof(argv[i + 1]);
		}

		if (found == 0) {
			val = strcmp(argv[i], "-r");
			if (val == 0) {
				found = 1;
				*reference_voltage = atof(argv[i + 1]);
			}
		}

		if (argc > 6) {
			if (found == 0) {
				val = strcmp(argv[i], "-e");
				if (val == 0) {
					found = 1;
					*allowed_error = atof(argv[i + 1]);
				}
			}

			if (found == 0) {
				val = strcmp(argv[i], "-r2");
				if (val == 0) {
					found = 1;
					*resistor_2 = atoi(argv[i + 1]);
				}
			}
			if (found == 0) {
				val = strcmp(argv[i], "-E");
				if (val == 0) {
					found = 1;
					*E_series = atoi(argv[i + 1]);
				}
			}
		}

		i = i + 2;
	}


	if ((*desired_voltage == 0) || (*reference_voltage == 0)){
		usage();
		return -1;
	}
	return 0;
}

int main(int argc, char** argv)
{

	int E96_values[96] = {
		100, 102, 105, 107, 110, 113, 115, 118, 121, 124, 127, 130, 133, 137,
		140, 143, 147, 150, 154, 158, 162, 165, 169, 174, 178, 182, 187, 191,
		196, 200, 205, 210, 215, 221, 226, 232, 237, 243, 249, 255, 261, 267,
		274, 280, 287, 294, 301, 309, 316, 324, 332, 340, 348, 357, 365, 374,
		383, 392, 402, 412, 422, 432, 442, 453, 464, 475, 487, 499, 511, 523,
		536, 549, 562, 576, 590, 604, 619, 634, 649, 665, 681, 698, 715, 732,
		750, 768, 787, 806, 825, 845, 866, 887, 909, 931, 953, 976
	};

	int E192_values[192] = {
		100, 101, 102, 104, 105, 106, 107, 109, 110, 111, 113, 114, 115, 117, 118, 
		120, 121, 123, 124, 126, 127, 129, 130, 132, 133, 135, 137, 138, 140, 142, 
		143, 145, 147, 149, 150, 152, 154, 156, 158, 160, 162, 164, 165, 167, 169, 
		172, 174, 176, 178, 180, 182, 184, 187, 189, 191, 193, 196, 198, 200, 203, 
		205, 208, 210, 213, 215, 218, 221, 223, 226, 229, 232, 234, 237, 240, 243,
		246, 249, 252, 255, 258, 261, 264, 267, 271, 274, 277, 280, 284, 287, 291,
		294, 298, 301, 305, 309, 312, 316, 320, 324, 328, 332, 336, 340, 344, 348,
		352, 357, 361, 365, 370, 374, 379, 383, 388, 392, 397, 402, 407, 412, 417,
		422, 427, 432, 437, 442, 448, 453, 459, 464, 470, 475, 481, 487, 493, 499,
		505, 511, 517, 523, 530, 536, 542, 549, 556, 562, 569, 576, 583, 590, 597,
		604, 612, 619, 626, 634, 642, 649, 657, 665, 673, 681, 690, 698, 706, 715,
		723, 732, 741, 750, 759, 768, 777, 787, 796, 806, 816, 825, 835, 845, 856,
		866, 876, 887, 898, 909, 920, 931, 942, 953, 965, 976, 988
	};


	double desired_voltage = 0;
	double reference_voltage = 0;
	double allowed_error = 0;
	int resistor_2 = 0;
	int resistor_1 = 0;
	int E_series = 96;
	
	int tmp = 0;
	int closest_index = 0;
	int limits = 0;
	int i = 0;
	int j = 0;

	double closest_value = HUGE_VAL;
	double ratio = 0;
	double ratio_tmp = 0;
	double error = 0;

	int* ptr = 0;

	if (parse_command_arguments(argc, argv, &desired_voltage, &reference_voltage, 
		&resistor_2, &allowed_error, &E_series) < 0) {
		return -1;
	}

	if (E_series == 192) {
		ptr = E192_values;
		limits = 192;
	}
	else if (E_series == 96) {
		ptr = E96_values;
		limits = 96;
	}
	else {
		printf("Invalid E_SERIES choice!");
		return -1;
	}

	ratio = desired_voltage / reference_voltage - 1;
	
	if (resistor_2 != 0) {
		resistor_1 = ratio * resistor_2;
		closest_index = 0;

		for (i = 0; i < limits; i++) {
			tmp = abs(resistor_1 - *(ptr + i));

			if (tmp < closest_value) {
				closest_value = tmp;
				closest_index = i;
			}
		}

		ratio = (double)(*(ptr + closest_index))/ (double)resistor_2 + 1;
		error = (reference_voltage * ratio - desired_voltage) * 100 / desired_voltage;

		printf("Resistor 1 = %d\n\r", *(ptr + closest_index));
		printf("Resistor 2 = %d\n\r", resistor_2);
		printf("Voltage = %f\n\r", reference_voltage * ratio);
		printf("Error = %f%%\n\r", error);
	}
	else {
		for (i = 0; i < limits; i++) {
			for (j = 0; j < limits; j++) {
				ratio_tmp = (double) *(ptr + j) / (double) *(ptr + i);
				ratio_tmp = fabs(ratio_tmp - ratio);
				if (ratio_tmp < closest_value) {
					resistor_1 = *(ptr + j);
					resistor_2 = *(ptr + i);
					closest_value = ratio_tmp;
				}
			}
		}
		ratio = (double) resistor_1 / (double) resistor_2 + 1;
		error = (reference_voltage * ratio - desired_voltage) * 100 / desired_voltage;
		printf("Resistor 1 = %d\n\r", resistor_1);
		printf("Resistor 2 = %d\n\r", resistor_2);
		printf("Voltage = %f\n\r", reference_voltage * ratio);
		printf("Error = %f%%\n\r", error);
	}
	

	return 0;
}
